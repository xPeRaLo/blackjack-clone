﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDealer // Every dealer must implement
{
    void GetCard();
    void CheckScore();
}
