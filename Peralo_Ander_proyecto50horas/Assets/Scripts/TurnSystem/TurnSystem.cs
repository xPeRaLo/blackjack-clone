﻿using System.Collections;
using UnityEngine;
public class TurnSystem : MonoBehaviour
{

	#region Variables
	public static TurnSystem _instance; // Only ONE instance of the turn system can be active.

    public Dealer dealer; // dealer

    [HideInInspector] public int currentPlayerTurn; // current player turn

    const float BLACKJACK_MULTIPLIER = 2.5f; // Multiplier if blackjack
    const int WIN_MULTIPLIER = 2; // Multiplier if player wins
    const int DRAW_MULTIPLIER = 1; // Multiplier if player draws with dealer

    const int MAX_PLAYERS = 4; // MAX PLAYERS allowed in game

    [HideInInspector] public float minimumBet = 100;
    [HideInInspector] public bool dealerDefeated = false;
    [HideInInspector] public bool dealerDefeatedBlackJack = false;
    [HideInInspector] public bool dealerDrawn = false;
    [HideInInspector] public bool gameStarted = false;
    #endregion

    void Awake()
    {
        #region Singleton
        if (_instance == null)
            _instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        #endregion
        
        DontDestroyOnLoad(gameObject);
    }

    #region Turn System functions
    public void StartGame()
    {
        Time.timeScale = 1;

        SetPlayers(); // Sets players
        ResetGame();
 

        UIHolder._instance.UpdatePlayerCurrency();
        UIHolder._instance.clearDealerCards();
        UIHolder._instance.clearUserCards();
    }


    public void ResetGame()
    {
        Time.timeScale = 1;

        dealerDefeated = false;
        dealerDefeatedBlackJack = false;
        dealerDrawn = false;
        gameStarted = true;

        resetDealer();
        ResetPlayers();
        SetUp();

        UIHolder._instance.playAnimation("Default");

        UIHolder._instance.UpdatePlayerCurrency();
        UIHolder._instance.clearDealerCards();
        UIHolder._instance.clearUserCards();
    }

	public void SetPlayers()
	{

        if(dealer.playerList.Count < 1 )
        {
            Player player = new Player();
            player.ResetPlayer();
            dealer.playerList.Add(player);
        }
 
        // CODE FOR MULTIPLE PLAYERS

        /*if ((int)UIHolder._instance.playerCount.value <= MAX_PLAYERS) // If its less than allowed
		{
			for (int i = 0; i < (int) UIHolder._instance.playerCount.value; i++) // Adds all players to list
			{
				Player player = new Player();
				player.ResetPlayer();
				dealer.playerList.Add(player);
				Debug.LogWarning("Player " + (i+1) + " ready");
			}
		}
		else
		{
			//Show a message to user warning about max players exceeded

			Debug.LogWarning("More players than allowed, max support is "+ MAX_PLAYERS);
			return;
		}*/
    }
    

    public IEnumerator NextTurn()
    {
       /* if (dealer.playerList[currentPlayerTurn].has2Decks && dealer.playerList[currentPlayerTurn].deck2_state == PLAYER_STATE.Inactive)
        {
            dealer.playerList[currentPlayerTurn].deck2_state = PLAYER_STATE.PlayerTurn;

            yield break;
        }
        else*/
            
        currentPlayerTurn++; // Increases turn

        yield return new WaitForSecondsRealtime(0.5f);

		UIHolder._instance.clearUserCards();
        UIHolder._instance.UpdateScore();

		if (dealer.state != DEALER_STATE.WaitingTurn)
		{
			CheckWinners();
		}

        if (currentPlayerTurn > dealer.playerList.Count - 1) // if new turn is higher than player count
        {
            dealer.state = DEALER_STATE.DealerTurn; // Dealer turn
            UIHolder._instance.HidePanel(UIHolder._instance.surrenderPanel);
            

            if (dealer.state == DEALER_STATE.DealerTurn)
                UIHolder._instance.playAnimation("CardPickDealerAnim");
        }
        else
            dealer.playerList[currentPlayerTurn].deck1_state = PLAYER_STATE.PlayerTurn; // Sets turn for new player

        
    }

    void SetUp() // Sets up game
    {
        currentPlayerTurn = 0; // sets turn to first player
        dealer.state = DEALER_STATE.WaitingTurn; // Dealer to waiting

        foreach(Player p in dealer.playerList) // Sets every player to wait state
        {
            p.deck1_state = PLAYER_STATE.WaitingTurn;
            
            if (p.Bet(float.Parse(UIHolder._instance.betAmountField.text)))
            {
                UIHolder._instance.ShowPanel(UIHolder._instance.gamePanel);
                UIHolder._instance.HidePanel(UIHolder._instance.restartPanel);
                UIHolder._instance.HidePanel(UIHolder._instance.startPanel);
            }  
          
        }

        dealer.playerList[currentPlayerTurn].deck1_state = PLAYER_STATE.PlayerTurn; // sets first player to active
    }

    void resetDealer()
    {
        dealer.ResetDealer();

    }

	void ResetPlayers()
	{
		foreach(Player p in dealer.playerList) // For each player player in list
		{
			p.deck1_CurrentScore = 0;
			p.cardDeck1.Clear();
            //p.cardDeck2.Clear();
        }
	}

    public void CheckWinners()
    {

        if (dealer.currentScore > dealer.MAX_SCORE_ALLOWED)
            dealer.state = DEALER_STATE.Eliminated;

        for(int i = 0; i < dealer.playerList.Count; i++) // For each player in list
        {

            Player p = dealer.playerList[i];

            if (p.deck1_state == PLAYER_STATE.Eliminated) // If its eliminated
            {
                p.ResetBet();
                dealerDefeated = false;
                dealerDefeatedBlackJack = false;
                dealerDrawn = false;
				continue; // Skips
            }

            if (dealer.state == DEALER_STATE.Eliminated) // If dealer out
            {
                if (p.deck1_state == PLAYER_STATE.BlackJack) // If player has blackJack
                {
                    p.Win(p.betAmount, BLACKJACK_MULTIPLIER); // Wins
                    dealerDefeated = false;
                    dealerDefeatedBlackJack = true;
                    dealerDrawn = false;
                    p.ResetBet();
                    continue;
                }

                if (p.deck1_state == PLAYER_STATE.Stand) // If it DOES NOT have but HAS NOT exceeded maximum score either
                {
                    p.Win(p.betAmount, WIN_MULTIPLIER); // Wins
                    dealerDefeated = true;
                    dealerDefeatedBlackJack = false;
                    dealerDrawn = false;
                    p.ResetBet();
                    continue;
                }
            }

            else // If dealer HAS NOT exceeded
            {
                if (p.deck1_state == PLAYER_STATE.BlackJack && dealer.state == DEALER_STATE.BlackJack) // If both have blackjack
                {
                    p.Win(p.betAmount, DRAW_MULTIPLIER); // Player gets back bet amount
                    dealerDefeated = true;
                    dealerDefeatedBlackJack = false;
                    dealerDrawn = false;
                    p.ResetBet();
                    continue;
                }

                if (p.deck1_state == PLAYER_STATE.BlackJack && dealer.state != DEALER_STATE.BlackJack) // If player has blackjack and dealer DOES NOT
                {
                    p.Win(p.betAmount, BLACKJACK_MULTIPLIER); // player gets blackjack multiplied bet amount
                    dealerDefeated = false;
                    dealerDefeatedBlackJack = true;
                    dealerDrawn = false;
                    p.ResetBet();
                    continue;
                }

                if (p.deck1_state == PLAYER_STATE.Stand && dealer.state == DEALER_STATE.Stand) // if neither have blackjack and HAVE NOT exceeded either
                {
                    if (p.getDeck1Score() == dealer.getCurrentScore()) // if both have same score
                    {
                        p.Win(p.betAmount, DRAW_MULTIPLIER); // player gets back bet amount
                        dealerDefeated = false;
                        dealerDefeatedBlackJack = false;
                        dealerDrawn = true;
                        p.ResetBet();
                        continue;
                    }
                    else if (p.getDeck1Score() > dealer.getCurrentScore()) // If player score is higher than dealer's
                    {
                        p.Win(p.betAmount, WIN_MULTIPLIER); // they get win multiplied bet amount
                        dealerDefeated = true;
                        dealerDefeatedBlackJack = false;
                        dealerDrawn = false;
                        p.ResetBet();
                        continue;
                    }
                    else // if player's score is lower than dealer's 
                    {
                        p.ResetBet();
                        dealerDefeated = false;
                        dealerDefeatedBlackJack = false;
                        dealerDrawn = false;
                        continue;
                    }
                }
            }
        }

        UIHolder._instance.EndGame();

       //Time.timeScale = 0;
       //IHolder._instance.ShowPanel(UIHolder._instance.restartPanel);
    }

	#endregion
	
    public int getMaxPlayers()
	{
		return MAX_PLAYERS;
	}
}
