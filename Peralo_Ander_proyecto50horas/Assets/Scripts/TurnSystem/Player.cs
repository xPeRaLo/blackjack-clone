﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PLAYER_STATE { PlayerTurn, WaitingTurn, Eliminated, BlackJack, Stand, Inactive }

[System.Serializable]
public class Player : IParticipant, ICurrency
{

	#region Variables

	public float currency = 5000; // Currency of the player needed to play.
	public int deck1_CurrentScore = 0; // the additive of all card values
	//public int deck2_CurrentScore = 0;
	
	public List<CardObject> cardDeck1 = new List<CardObject>(); // All cards in the player's 1st deck
	public PLAYER_STATE deck1_state; // State of the player's 1st deck

	//public List<CardObject> cardDeck2 = new List<CardObject>();
	//public PLAYER_STATE deck2_state;
	
	[HideInInspector] public int MAX_SCORE_ALLOWED = 21;

	[HideInInspector] public float betAmount = 0; // Bet amount, will be implemented later
	[HideInInspector] public float previousBetAmount;
	[HideInInspector] public bool has2Decks = false;
	[HideInInspector] public bool can2Deck = true;

	#endregion

	#region Player functions

	public void GetCard(List<CardObject> cards, PLAYER_STATE state) // Generates a card for the player
	{

		GameObject cardpf = CardSystem._instance.GenerateCard(); // Generates card
		CardObject card = cardpf.GetComponent<CardObject>(); // Gets cardObject component

		cards.Add(card); // Adds it to the list

		CheckScore(cards, state); // Check Score
	}

	public void CheckScore(List<CardObject> cards, PLAYER_STATE state) // Check score
	{
		if (state == PLAYER_STATE.Eliminated || state == PLAYER_STATE.BlackJack || state == PLAYER_STATE.Stand) // if turn is over return
			return;

		
		deck1_CurrentScore += cards[cards.Count - 1].gameObject.GetComponent<CardObject>().cardValue; // Updates score
		UIHolder._instance.UpdateScore(); //Updates UI
		

		if (deck1_CurrentScore == MAX_SCORE_ALLOWED && cards.Count == 2) // If user has 21, and 2 cards
		{
			deck1_state = PLAYER_STATE.BlackJack; // BlackJack
			TurnSystem._instance.StartCoroutine(TurnSystem._instance.NextTurn()); // Next Turn
			return;
		}

		if(deck1_CurrentScore > MAX_SCORE_ALLOWED) // If exceeds 21
		{
			deck1_state = PLAYER_STATE.Eliminated; // Eliminated from game
			TurnSystem._instance.StartCoroutine(TurnSystem._instance.NextTurn()); // Next turn
			return;
		}

		if(deck1_CurrentScore == 21 && cards.Count > 2) // If score is 21 with more that two cards
		{
			deck1_state = PLAYER_STATE.Stand; // Cannot play anymore, has to wait
			TurnSystem._instance.StartCoroutine(TurnSystem._instance.NextTurn()); // Next turn
			return;
		}

		if (cards.Count >= 2 && state == PLAYER_STATE.PlayerTurn) // If list of cards is greater than 2 and it is his turn
			UIHolder._instance.ShowPanel(UIHolder._instance.surrenderPanel); // Shows panel to surrender
	}

	public void EnterStandState() // Enter surrender state function
	{
		if (deck1_state != PLAYER_STATE.Stand)
		{
			deck1_state = PLAYER_STATE.Stand;
		}

		TurnSystem._instance.StartCoroutine(TurnSystem._instance.NextTurn());
	}

	public void ResetPlayer() // Resets player
	{
		can2Deck = true;
		has2Decks = false;
		deck1_CurrentScore = 0;
		
		betAmount = 0;
		cardDeck1.Clear();
		deck1_state = PLAYER_STATE.WaitingTurn;


	}

	public int getDeck1Score()
	{
		return deck1_CurrentScore;
	}


	#region Betting and profit functions, go with CURRENCY and CURRENTBET variable 

	public void Win(float betAmount, float multiplier) // Win / Draw function
	{
		currency += (betAmount * multiplier); // he receives back the win amount multiplied by a win multiplier
		UIHolder._instance.UpdatePlayerCurrency();
	}

	public bool Bet(float betAmount) // Bet Function
	{

		if (currency - betAmount >= 0)
		{
			this.betAmount += betAmount; // current bet variable sets to parameter
			currency -= betAmount; // Reduce the currency with the bet amount

			return true;
			
		}
		else
		{
			Debug.LogWarning("Not enough Money");
			return false;
		}
		

		
	}

	public void ResetBet() // Reset bet function
	{
		betAmount = 0;
	}

	public void Split()
	{
		/*cardDeck2.Add(cardDeck1[1]);
		cardDeck1.RemoveAt(1);*/

		Bet(betAmount);
	}

	public void ResetCurrency()
	{
		currency = 5000;
	}

	#endregion

	public void SetAceCardValue(int value) // Set ace card function
	{
		if (deck1_state == PLAYER_STATE.PlayerTurn)
		{

			cardDeck1[cardDeck1.Count - 1].gameObject.GetComponent<CardObject>().cardValue = value; // Gets last card in the list and sets value

			CheckScore(cardDeck1, deck1_state); // Check score

			return;
		}

		/*if(deck2_state == PLAYER_STATE.PlayerTurn)
		{
			cardDeck2[cardDeck2.Count - 1].gameObject.GetComponent<CardObject>().cardValue = value; // Gets last card in the list and sets value

			Debug.Log(cardDeck2[cardDeck1.Count - 1].gameObject.GetComponent<CardObject>().cardValue); // Debug.

			CheckScore(cardDeck2, deck2_state); // Check score

			return;
		}*/
	
	}

	#endregion
}
