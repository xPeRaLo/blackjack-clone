﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DEALER_STATE { DealerTurn, WaitingTurn, Eliminated, BlackJack, Win, Stand }

[System.Serializable]
public class Dealer : IDealer
{
	#region Variables

	public int currentScore = 0; // Current score of the dealer
	
	public List<CardObject> cardList; // A list to store the cards he's picking
	public List<Player> playerList; // A list storing all the players in the game
	public DEALER_STATE state; // State of the dealer

	[HideInInspector] public int MAX_SCORE_ALLOWED = 21; // Maximun score allowed to win
	[HideInInspector] public int MAX_SCORE_ALLOWED_FOR_NEW_CARD = 16; // Maximum score a dealer can have to pick another card

	const float PICK_CARD_PERCENTAGE = 20f; // percetange of times dealer will pick up card. KEEP BETWEEN 0-100.

	[HideInInspector] public bool canPlayPickAnim = true;
	#endregion

	public Dealer() // Constructor
	{
		playerList = new List<Player>(); // Initializes player list

		playerList.Clear(); // Clears it in case there is old data.
	}


	#region Dealer functions

	public void GetCard() // Generates a card for the player
	{

		if (state != DEALER_STATE.DealerTurn) // If it's not dealer turn
			return; // Returns

		if (!canPlayPickAnim)
			return;

		if (!PickCardAgain()) // If does not pick card 
		{
			EnterHitState(); // Enters stand state


		}
		else
		{

			GameObject cardpf = CardSystem._instance.GenerateCard(); // Generates card
			CardObject card = cardpf.GetComponent<CardObject>(); // Gets card object component from generated card

			if (card.cardNumber == CardNumber.Ace) // If it's ace
			{

				UIHolder._instance.HidePanel(UIHolder._instance.acePanel); // Dealer does not need to let user choose ace value

				if (currentScore < 11) // if his current score is under 11
				{
					card.cardValue = 11; // He'll set it to 11
				}
				else // If not
				{
					card.cardValue = 1; // 1
				}
			}

			cardList.Add(card); // Add card to list
			
		}

		CheckScore(); // Check Score
	}

	public void CheckScore() // Check score
	{

		if (state == DEALER_STATE.Eliminated || state == DEALER_STATE.BlackJack || state == DEALER_STATE.Stand) // if turn is over return
			return;

		currentScore += cardList[cardList.Count - 1].gameObject.GetComponent<CardObject>().cardValue; // Increments score

		UIHolder._instance.UpdateScore(); // Updated UI


		if (currentScore == MAX_SCORE_ALLOWED && cardList.Count == 2) // If dealer has 21, and 2 cards
		{
			state = DEALER_STATE.BlackJack; // BlackJack
			TurnSystem._instance.StartCoroutine(TurnSystem._instance.NextTurn());
			return;
		}

		if (currentScore > MAX_SCORE_ALLOWED) // If exceeds 21
		{
			state = DEALER_STATE.Eliminated; // Eliminated
			TurnSystem._instance.StartCoroutine(TurnSystem._instance.NextTurn());
			return;
		}

		if (currentScore == 21 && cardList.Count > 2 && state != DEALER_STATE.Eliminated) // If score is 21 with more that two cards
		{
			state = DEALER_STATE.Stand; // Cannot play anymore, has to wait
			TurnSystem._instance.StartCoroutine(TurnSystem._instance.NextTurn());
			return;
		}		
	}

	public void EnterHitState() // Enter surrender state function
	{
		if (state != DEALER_STATE.Stand)
		{
			UIHolder._instance.playAnimation("Default");
			state = DEALER_STATE.Stand;
			TurnSystem._instance.StartCoroutine(TurnSystem._instance.NextTurn());
		}
	}

	public void ResetDealer() // Resets dealer
	{
		currentScore = 0;
		cardList.Clear();
		state = DEALER_STATE.WaitingTurn;

	}

	public int getCurrentScore()
	{
		return currentScore;
	}

	bool PickCardAgain() 
	{
		if (currentScore >= MAX_SCORE_ALLOWED_FOR_NEW_CARD) // If score is higher than allowed for new card
			return false; // returns false, CAN NOT pick card

		int highestScoreToBeat = 0; // variable to keep track what the highest score is


		foreach(Player p in playerList) // For each player
		{
			if (highestScoreToBeat < p.getDeck1Score() && p.getDeck1Score() <= p.MAX_SCORE_ALLOWED) // If current high score is less than new high score and has NOT exceeded max allowed score
				highestScoreToBeat = p.getDeck1Score(); // Set new high score
		}

		if (highestScoreToBeat < MAX_SCORE_ALLOWED_FOR_NEW_CARD)
			highestScoreToBeat = MAX_SCORE_ALLOWED;

		if (highestScoreToBeat > currentScore) // if highest score is bigger than dealer's current score
		{
			if (currentScore < (highestScoreToBeat - 3) && currentScore <= MAX_SCORE_ALLOWED_FOR_NEW_CARD)
				return true;

			
				float pickCard = Random.Range(0, 100); // generates a float representing percentage

				if (pickCard <= PICK_CARD_PERCENTAGE) // if it is less than x number
					return false; // DOES NOT PICK CARD, returns false
				else
					return true; // Picks card, returns true	
		}
		return true;
	}
	#endregion
}
