﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IParticipant // Dealer and players must implement
{
   void GetCard(List<CardObject> cards, PLAYER_STATE state);
   void CheckScore(List<CardObject> cards, PLAYER_STATE state);


}
