﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICurrency // Every player must implement
{

    void Win(float betAmount, float multiplier);
    bool Bet(float betAmount);
    void Split(); // Method for spliting in case of two equal cards, NOT IMPLEMENTED
    
}
