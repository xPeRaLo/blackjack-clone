﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
public class UIHolder : MonoBehaviour
{

	#region Variables

	public static UIHolder _instance; // Static instance

	// Load all UI References
	[Header("Main panels")]

	public GameObject startPanel;
	public GameObject gamePanel;

	[Space]

	[Header("Starting screen elements")]

	public Slider playerCount;
	public Text playerCountText;
	public TMP_InputField betAmountField;

	[Space]

	[Header("Game screen elements")]

	public GameObject acePanel;
	public GameObject surrenderPanel;
	public GameObject scorePanel;
	public GameObject restartPanel;
	//public GameObject splitPanel;

	public TextMeshProUGUI currentPlayerScoreText;
	public TextMeshProUGUI currentPlayerCurrency;
	public TextMeshProUGUI youXtxt;

	public GameObject btnPickCard;
	public GameObject btnStand;

	[Space]


	public GameObject[] deck1Positions;
	public GameObject[] deck2Positions;

	public GameObject cardPicker;
	Animator cardPickerAnim;


	[HideInInspector] public int maxSpawnableCardsPlayer = 9;
	[HideInInspector] public int maxSpawnableCardsDealer = 9;

	int spawnedCardsPlayers = 0;
	int spawnedCardsDealer = 0;

	#endregion

	void Awake()
	{
		#region Singleton
		if (_instance == null)
			_instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}
		#endregion

		startPanel.SetActive(true);
		gamePanel.SetActive(false);
		restartPanel.SetActive(false);

		acePanel.SetActive(false);
		surrenderPanel.SetActive(true);
		restartPanel.SetActive(false);

		// Show finalPanel
		scorePanel.SetActive(true);

		cardPickerAnim = cardPicker.GetComponent<Animator>();
	}

	private void Start()
	{

		if (TurnSystem._instance.dealer.playerList.Count > 0)
		{
			UpdatePlayerCurrency();
			UpdateScore();
		}
	}

	private void Update()
	{
		if(Input.GetButtonDown("Cancel"))
			
			if(TurnSystem._instance.gameStarted)
				
				if (restartPanel.activeSelf)
				{
					HidePanel(restartPanel);
					Time.timeScale = 0;
				}
				else
				{
					ShowPanel(restartPanel);
					Time.timeScale = 1;
				}	
	}

	#region Functions/Methods/Coroutines
	public void UpdatePlayerCountText()
	{
		playerCountText.text = ((int)playerCount.value).ToString();
	}

	public void ShowPanel(GameObject panelToShow)
	{
		panelToShow.SetActive(true);
	}
	public void HidePanel(GameObject panelToHide)
	{
		panelToHide.SetActive(false);
	}

	public void setActive(GameObject go)
	{
		Button btn = go.GetComponent<Button>();

		if (btn != null)
			btn.interactable = true;
		else
			gameObject.SetActive(true);
	}

	public void setInactive(GameObject go)
	{
		Button btn = go.GetComponent<Button>();

		if (btn != null)
			btn.interactable = false;
		else
			gameObject.SetActive(false);
	}

	public void EndGame()
	{
		TurnSystem._instance.gameStarted = false;

		Time.timeScale = 0; // Pauses game

		//Set text & color
		if (TurnSystem._instance.dealerDefeated) // if dealer lost
		{
			youXtxt.color = Color.green;
			youXtxt.text = "YOU WIN";
			AudioManager.instance.Play("Win");
		}
		else if(TurnSystem._instance.dealerDefeatedBlackJack) // If dealer lost by blackJack
		{
			youXtxt.color = Color.green;
			youXtxt.text = "BLACKJACK";
			AudioManager.instance.Play("BlackJack");

		}
		else if(TurnSystem._instance.dealerDrawn) // If they draw
		{
			youXtxt.color = new Color(255, 140, 0); //Orange color
			youXtxt.text = "DRAW";
		}

		else if(!TurnSystem._instance.dealerDefeated &&
				!TurnSystem._instance.dealerDefeatedBlackJack &&
				!TurnSystem._instance.dealerDrawn) // If dealer won
		{
			youXtxt.color = Color.red;
			youXtxt.text = "YOU LOSE";
			AudioManager.instance.Play("Game Over");
		}


		//Show Panels


		acePanel.SetActive(false);
		surrenderPanel.SetActive(false);
		restartPanel.SetActive(true);

		// Show Score
		scorePanel.SetActive(true);
	}

	public void StartGame()
	{
		/*if((int)playerCount.value < 1)
		{
			Debug.Log("At least 1 player needed.");
			return;
		}*/



		/*if(TurnSystem._instance.dealer.playerList[0].betAmount < TurnSystem._instance.minimumBet ||
			TurnSystem._instance.dealer.playerList[0].betAmount > TurnSystem._instance.dealer.playerList[0].currency)
		{
			return;
		}*/

		if ((int)playerCount.value > TurnSystem._instance.getMaxPlayers())
		{
			Debug.LogWarning("Exceeded max player count. Maximum players are " + TurnSystem._instance.getMaxPlayers());
			return;
		}

		if (float.Parse(betAmountField.text) < TurnSystem._instance.minimumBet)
		{
			Debug.LogWarning("Minimum bet is 100");
			return;
		}

		clearUserCards();
		UpdateScore();

		if (Time.timeScale < 1)
			Time.timeScale = 1;

		resetUIScore();

		TurnSystem._instance.StartGame();
		/*if (TurnSystem._instance.dealer.playerList[0].Bet(TurnSystem._instance.dealer.playerList[0].betAmount))
		{

			
			Debug.Log("Game Start");

			startPanel.SetActive(false);
			gamePanel.SetActive(true);
			restartPanel.SetActive(false);
		}else
		{
			Debug.Log("Game cant Start");
			return;
		}*/

	}

	public void ChangeBet()
	{
		Time.timeScale = 1;

		startPanel.SetActive(true);
		gamePanel.SetActive(false);
		restartPanel.SetActive(false);

		acePanel.SetActive(false);
		surrenderPanel.SetActive(true);
		restartPanel.SetActive(false);

		// Show finalPanel
		scorePanel.SetActive(true);
	}

	public void GetCard()
	{

		//PlAY CARD ANIM

		if (cardPickerAnim != null)
			cardPickerAnim.Play("CardPickerAnim");
		else
		{
			Debug.LogWarning("No animator detected, can NOT play animations for card Picker");
			return;
		}


		/*if(TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].deck2_state == PLAYER_STATE.PlayerTurn)
		{
			TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].GetCard(TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].cardDeck2,
																								   TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].deck2_state);
			return;
		}*/
	}

	public void EnterStandState()
	{
		TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].EnterStandState();
	}

	public void SetAceValue(int value)
	{
		//Access the last element added to the list and set a value

		CardSystem._instance.SetAceValue(value);

		acePanel.SetActive(false);
	}

	public void SpawnPlayerCards(GameObject card)
	{

		if (spawnedCardsPlayers >= maxSpawnableCardsPlayer)
		{
			Debug.LogWarning("Can NOT spawn more cards");
			return;
		}

		GameObject spawned = Instantiate(card, deck1Positions[spawnedCardsPlayers].transform);
		spawnedCardsPlayers++;

	}

	public void SpawnDealerCards(GameObject card)
	{
		if (spawnedCardsDealer >= maxSpawnableCardsDealer)
		{
			Debug.LogWarning("Can NOT spawn more cards");
			return;
		}

		GameObject spawned = Instantiate(card, deck2Positions[spawnedCardsDealer].transform);
		spawnedCardsDealer++;
	}

	public void clearUserCards()
	{
		spawnedCardsPlayers = 0;

		for (int i = 0; i < deck1Positions.Length; i++)
		{
			foreach (Transform g in deck2Positions[i].transform)
			{
				Destroy(g.gameObject);
			}
		}
	}

	public void clearDealerCards()
	{
		spawnedCardsDealer = 0;

		for (int i = 0; i < deck2Positions.Length; i++)
		{
			foreach (Transform g in deck1Positions[i].transform)
			{
				Destroy(g.gameObject);
			}
		}
	}

	public void UpdateScore()
	{

		if (TurnSystem._instance.currentPlayerTurn <= TurnSystem._instance.dealer.playerList.Count - 1)
			currentPlayerScoreText.text = "Score: " + TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].deck1_CurrentScore;
		else
			currentPlayerScoreText.text = "Score: " + TurnSystem._instance.dealer.currentScore;
	}

	public void resetUIScore()
	{
		currentPlayerScoreText.text = "Score: 0";
	}

	public void UpdatePlayerCurrency()
	{
		currentPlayerCurrency.text = " " + TurnSystem._instance.dealer.playerList[0].currency.ToString();
	}

	public void ResetGame()
	{
		Time.timeScale = 1;

		acePanel.SetActive(false);
		surrenderPanel.SetActive(true);
		restartPanel.SetActive(false);

		// Show finalPanel
		scorePanel.SetActive(true);


		clearUserCards();
		UpdateScore();
		resetUIScore();

		TurnSystem._instance.ResetGame();
	}

	public void playAnimation(string animationName)
	{
		cardPickerAnim.Play(animationName);
	}

	#endregion
}
