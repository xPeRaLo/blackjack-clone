﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadScene(int lvl)
    {
        SceneManager.LoadScene(lvl);
    }
    public void ReturnMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void Quit()
    {
       // if(TurnSystem._instance.dealer.playerList.Count > 0)
       //     TurnSystem._instance.dealer.playerList.Clear();
        
        Application.Quit();
    }

}
