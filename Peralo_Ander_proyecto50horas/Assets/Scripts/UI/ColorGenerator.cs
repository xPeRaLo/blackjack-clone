﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class ColorGenerator : MonoBehaviour
{

    public TMP_ColorGradient [] gradients; // Color gradient array

    public TextMeshProUGUI text;


    float currentTimer;
    float maxTimer = 1.25f;


    private void Start()
    {
        currentTimer = maxTimer;
    }

    private void Update()
    {
        if (gradients.Length < 1) // if no elements in array
        {
            Debug.Log("No elements in array");
            return;
        }


        if (currentTimer > 0)
            currentTimer -= Time.deltaTime;
        
        else
        {
            generateColor();

            currentTimer = maxTimer;
        }
    }

    void generateColor()
    {
        int random = Random.Range(0, gradients.Length);

        TMP_ColorGradient gradient = gradients[random];
        text.colorGradientPreset = gradient;
    }
}