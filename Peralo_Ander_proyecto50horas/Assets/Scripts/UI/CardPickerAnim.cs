﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardPickerAnim : MonoBehaviour
{

    public GameObject backsideCardpf;
    public Transform pointToSpawn;

    public void SpawnBacksideCard(float timeToDeleteCard)
    {
       

        GameObject go = Instantiate(backsideCardpf, pointToSpawn);

        Destroy(go, timeToDeleteCard);
        
    }

    public void PlayDealerAnim() // DEALER ANIMATION, FOR SOME FUCKING UNKNOWN REASON, PLAYS DEALER ANIMATION, EVEN IF HE DOES NOT GET TO PICK A CARD, ONLY WHEN USER SCORE IS HIGHER.
                                 // NOT REALLY GAMEBREAKING, BUT CAN BOTHER SOME PICKY RETARDED USERS.
    {
        if(TurnSystem._instance.dealer.state != DEALER_STATE.DealerTurn)
        {
            UIHolder._instance.playAnimation("Default");
            return;
        }


        if (TurnSystem._instance.dealer.state == DEALER_STATE.DealerTurn)
            UIHolder._instance.playAnimation("CardPickDealerAnim");
        

    }

    public void SpawnCard()
    {
        if (TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].deck1_state == PLAYER_STATE.PlayerTurn)
        {
            TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].GetCard(TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].cardDeck1,
                                                                                                   TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].deck1_state);
            return;
        }
    }

    public void setDealerAnimState(int i)
    {
        if (TurnSystem._instance.dealer.state == DEALER_STATE.DealerTurn)
        {

            if (i == 0)
                TurnSystem._instance.dealer.canPlayPickAnim = false;
            else if (i == 1)
                TurnSystem._instance.dealer.canPlayPickAnim = true;
            else
                Debug.LogWarning("Use 0 or 1 for false or true, that's the purpose of this parameter, to act as bool");
        }
    }

    public void SpawnDealerCard()
    {

        TurnSystem._instance.dealer.GetCard();
        
    }

    public void SetButtonsInactive()
    {
        UIHolder._instance.setInactive(UIHolder._instance.btnPickCard);
        UIHolder._instance.setInactive(UIHolder._instance.btnStand);
    }

    public void SetButtonsActive()
    {
        UIHolder._instance.setActive(UIHolder._instance.btnPickCard);
        UIHolder._instance.setActive(UIHolder._instance.btnStand);
    }

}
