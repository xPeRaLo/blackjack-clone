﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CardSystem : MonoBehaviour
{
    public GameObject cardpf; // Card prefab to spawn
    public GameObject backSideCardpf;

    public Card[] cards; // Array containing all cards in the deck

    public static CardSystem _instance; // ONLY one instance can be in the scene

    void Awake()
    {
		#region Singleton
		if (_instance == null)
        {
            _instance = this;
           
        }
        else
            Destroy(gameObject);
		#endregion
	}


	public GameObject GenerateCard() //Generates card
    {
        GameObject pf = cardpf; // set prefab to new gameobject
		CardObject obj = cardpf.GetComponent<CardObject>(); // get cardobject component from prefab

		int randomCard = Random.Range(0, cards.Length); // get a random card in the deck
		
		obj.SetUp(cards[randomCard]); // Set up obtained card


        if (TurnSystem._instance.dealer.state == DEALER_STATE.WaitingTurn) // If player turn
            UIHolder._instance.SpawnPlayerCards(cardpf); // Spawn created card on player Side
        else
            UIHolder._instance.SpawnDealerCards(cardpf); // Spawn on dealer side

       

            

		if (obj.cardNumber == CardNumber.Ace)
		{
			UIHolder._instance.ShowPanel(UIHolder._instance.acePanel); // Show Ace value selection panel
			Time.timeScale = 0; // Pause game

		}

		return pf; // Returns created gameobject
        
    }

    public void SetAceValue(int value) // Sets value for ace card
    {
		Time.timeScale = 1; // Resumes game
        TurnSystem._instance.dealer.playerList[TurnSystem._instance.currentPlayerTurn].SetAceCardValue(value); // Sets corresponding player's card's value.
    }
}
