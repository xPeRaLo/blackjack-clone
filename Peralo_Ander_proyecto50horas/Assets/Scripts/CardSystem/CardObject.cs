﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardObject : MonoBehaviour
{
	// Card object class


	Image cardImage;

	public CardType cardType;
	public CardNumber cardNumber;
	public int cardValue;


	public void SetUp(Card card) // Sets up card attributes
	{

		cardImage = gameObject.GetComponent<Image>();

		cardImage.sprite = card.cardSprite;

		cardType = card.cardType;
		cardNumber = card.cardNumber;
		cardValue = card.cardValue;

		
	}

}
