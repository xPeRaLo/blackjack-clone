﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum CardType { Spades, Hearts, Diamonds, Clovers }
public enum CardNumber { Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, J, K, Q }

// Card preset containing basic elements of a card

public class Card: ScriptableObject
{

	public CardType cardType;
	public CardNumber cardNumber;

	[Space]
	public int cardValue;
	public Sprite cardSprite;

}
