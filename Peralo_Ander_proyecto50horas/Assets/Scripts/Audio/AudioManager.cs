﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;
public class AudioManager : MonoBehaviour
{
    public Sound[] sounds; // Custom sound class array for sounds

    public static AudioManager instance; // static instance

    private void Awake()
    {
		#region Singleton
		if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
		#endregion

		DontDestroyOnLoad(gameObject); // Persists on scene switch

        foreach(Sound s in sounds) // For each audio in sounds array
        {
            s.source = gameObject.AddComponent<AudioSource>(); // Adjust audio settings
            
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.playOnAwake = s.playOnAwake;
            s.source.outputAudioMixerGroup = s.group;

        }

        Play("Main Theme");
    }

    public void Play(string name) // Play function for audios
    {
        Sound s =  Array.Find(sounds, sound => sound.name == name); // find in sounds array the audio with the name passed in parameter

        if (s == null) // if no audio found throws warning
        {
            Debug.LogWarning("Sound " + name + " not found");
            return;
        }

        s.source.Play(); // call play on the audioSource element
    }
}
